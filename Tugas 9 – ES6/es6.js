// 1
const golden = goldenfunction = () => {
  console.log('this is golden')
}

// 2
const newFunction = (firstName, lastName)=>{
  return {
    firstName,
    lastName,
    fullName: function () {
      console.log(firstName + " " + lastName)
      return
    }
  }
}
newFunction("William", "Imoh").fullName() 

// 3
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName,lastName,destination,occupation,spell} = newObject
console.log(firstName, lastName, destination, occupation)

// 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

// 5
const planet = "earth"
const view = "glass"
var before = `lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.`
console.log(before) 