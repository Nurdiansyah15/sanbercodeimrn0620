// No 1
class Animal {
  constructor(name) {
    this._name = name
    this._legs = 4
    this._cold_blooded = false
  }
  get name() {
    return this._name
  }
  get legs() {
    return this._legs
  }
  get cold_blooded() {
    return this._cold_blooded
  }
}
var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
  constructor(name) {
    super(name)
    this._legs = 2
  }
  yell() {
    return 'Auooo'
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name)
  }
  jump() {
    return 'hop hop'
  }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell())  // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 

// sola No 2
class Clock {
  constructor(template) {
    this.template = template.template
    this.timer
  }
  render() {
    this.date = new Date();
    this.hours = this.date.getHours();
    if (this.hours < 10) this.hours = '0' + this.hours;

    this.mins = this.date.getMinutes();
    if (this.mins < 10) this.mins = '0' + this.mins;

    this.secs = this.date.getSeconds();
    if (this.secs < 10) this.secs = '0' + this.secs;

    this.output = this.template
      .replace('h', this.hours)
      .replace('m', this.mins)
      .replace('s', this.secs);

    console.log(this.output);
  }
  stop() {
    clearInterval(timer);
  }
  start() {
    this.render()
    this.timer = setInterval(this.render.bind(this), 1000)
  }
}
// function Clock({ template }) {

//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = '0' + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = '0' + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = '0' + secs;

//     var output = template
//       .replace('h', hours)
//       .replace('m', mins)
//       .replace('s', secs);

//     console.log(output);
//   }

//   this.stop = function() {
//     clearInterval(timer);
//   };

//   this.start = function() {
//     render();
//     timer = setInterval(render, 1000);
//   };

// }

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 
