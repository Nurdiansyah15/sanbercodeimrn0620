function balikString(kata) {
  kata2 = []
  for (i = 0; i < kata.length; i++) {
    kata2.unshift(kata[i])
  }
  kata3 = ''
  for (k = 0; k <= kata2.length - 1; k++) {
    kata3 += kata2[k]
  }
  return kata3.toUpperCase()
}

function palindrome(kata) {
  kata2 = []
  kata.toUpperCase()
  for (i = 0; i < kata.length; i++) {
    kata2.unshift(kata[i])
  }
  kata3 = ''
  for (k = 0; k <= kata2.length - 1; k++) {
    kata3 += kata2[k]
  }
  palind = false
  if (kata3 === kata) {
    palind = true
  }
  else {
    palind = false
  }
  return palind
}

function bandingkan(num1 = 0, num2 = 0) {
  keluar = ''
  if (num1 <= 0 || num2 <= 0 || num1 === num2) {
    keluar = -1
  }
  else if (num1 != num2) {
    if (num1 > num2) {
      keluar = num1
    }
    else if (num1 < num2) {
      keluar = num2
    }
  }
  return keluar
}

// TEST CASES BalikString

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
console.log(balikString("#################################"))

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
console.log(balikString("#################################"))

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18