// // no 1 looping While
console.log('LOOPING PERTAMA')
i = 2
while (i <= 20) {
  console.log(i + ' - I love codng')
  i += 2
}
console.log('LOOPING KEDUA')
j = 20
while (j >= 2) {
  console.log(j + ' - I will become a mobile developer')
  j -= 2
}

// no 2 looping for
for (k = 1; k <= 20; k++) {
  if (k % 2 == 0) {
    console.log(k + ' - berkualitas')
  }
  else if (k % 2 != 0) {
    if (k % 3 == 0) {
      console.log(k + ' - I love coding')
    }
    else {
      console.log(k + ' - santai')
    }
  }
}

// no 3 membuat persegi panjang
for (i = 1; i <= 4; i++) {
  console.log('########')
}

// no 4 membuat tangga
for (i = 1; i <= 7; i++) {
  panggar = '#'
  console.log(panggar.repeat(i))
}

// no 5 membuat papan catur
for (i = 1; i <= 4; i++) {
  console.log(' # # # #')
  console.log('# # # # ')
}