// no 1
function range(startNum,finishNum){
  isiArray = []
  if (startNum<finishNum){
    for (i=startNum;i<=finishNum;i++){
      isiArray.push(i)
    }
  }
  else if (startNum>finishNum){
    for (i=startNum;i>=finishNum;i--){
      isiArray.push(i)
    }
  }
  else if (startNum==null || finishNum==null){
    isiArray=-1
  }
  return isiArray
}

console.log(range(10))

// no 2
function rangeWithStep(startNum,finishNum,step){
  isiArray = []
  if (startNum<finishNum){
    for (i=startNum;i<=finishNum;i=i+step){
      isiArray.push(i)
    }
  }
  else if (startNum>finishNum){
    for (i=startNum;i>=finishNum;i=i-step){
      isiArray.push(i)
    }
  }
  else if (startNum==null || finishNum==null){
    isiArray=-1
  }
  return isiArray
}
console.log(rangeWithStep(1,10,1))

// no 3
function sum(startNum,finishNum,step){
  isiArray=0
  if (startNum<finishNum && step!=null){
    for (i=startNum;i<=finishNum;i=i+step){
      isiArray=isiArray+i
    }
  }
  else if (startNum>finishNum && step!=null){
    for (i=startNum;i>=finishNum;i=i-step){
      isiArray=isiArray+i
    }
  }
  if (startNum<finishNum && step==null){
    for (i=startNum;i<=finishNum;i++){
      isiArray=isiArray+i
    }
  }
  else if (startNum>finishNum && step==null){
    for (i=startNum;i>=finishNum;i--){
      isiArray=isiArray+i
    }
  }
  else if (startNum==null && finishNum==null && step==null){
    isiArray=0
  }
  else if (finishNum==null){
    isiArray=startNum
  }
  return isiArray
}
console.log(sum())

// no 4
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(masukan) {

  data = []
  for (i = 0; i <= masukan.length - 1; i++) {
    data.push(
      ['No Id : ' + masukan[i][0],
      'Nama Lengkap : ' + masukan[i][1],
      'Alamat : ' + masukan[i][2],
      'Tanggal Lahir : ' + masukan[i][3],
      'Hobi : ' + masukan[i][4]
      ]
    )
  }
  return data
}

console.log(dataHandling(input))

//no 5
kata = 'nama eko'

function balikKata(kata){
  kata2=[]
  for (i=0;i<kata.length;i++){
    kata2.unshift(kata[i])
  }
  kata3=''
  for(k=0;k<=kata2.length-1;k++){
    kata3 +=kata2[k]
  }
  return kata3.toUpperCase()
}

console.log(balikKata(kata))

// no 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log(dataHandling2(input))

function dataHandling2(input) {
  input.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
  console.log(input)
  split = input[3].split('/')
  bulan = Number(split[1])
  switch (bulan) {
    case 1: {
      bulan = 'Januari'
      break;
    }
    case 2: {
      bulan = 'Februari'
      break;
    }
    case 3: {
      bulan = 'Maret'
      break;
    }
    case 4: {
      bulan = 'April'
      break;
    }
    case 5: {
      bulan = 'Mei'
      break;
    }
    case 6: {
      bulan = 'Juni'
      break;
    }
    case 7: {
      bulan = 'Juli'
      break;
    }
    case 8: {
      bulan = 'Agustus'
      break;
    }
    case 9: {
      bulan = 'September'
      break;
    }
    case 10: {
      bulan = 'Oktober'
      break;
    }
    case 11: {
      bulan = 'November'
      break;
    }
    case 12: {
      bulan = 'Desember'
      break;
    }
  }
  console.log(bulan)
  console.log(split.sort(function (value1, value2) { return value2 - value1 } ))
  console.log(split.join('-'))
  console.log(input[1].slice(0,15)) 
  return 
}


/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */