// Tugas if else

const nama = "Nurdiansyah"
const peran = "Werewolf"

if (nama) {
  if (peran == "Penyihir") {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!')
  } else if (peran == 'Guard') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo Guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf')
  } else if (peran == 'Werewolf') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo Werewolf ' + nama + ', Kamu akan memakan mangsa setiap malam!')
  } else {
    console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!')
  }
} else {
  console.log('Nama harus diisi!')
}

console.log("#####################################################")

//tugas switch
tanggal = 9
bulan = 9
tahun = 2000

switch (true) {
  case (tanggal < 1 || tanggal > 31): {
    console.log('Tanggal yang anda masukan salah')
    break;
  }
  default: {
    switch (true) {
      case (tahun < 1900 || tahun > 2200): {
        console.log('Tahun yang anda masukan salah')
        break;
      }
      default: {
        switch (bulan) {
          case 1: {
            bulan = 'Januari'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 2: {
            bulan = 'Februari'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 3: {
            bulan = 'Maret'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 4: {
            bulan = 'April'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 5: {
            bulan = 'Mei'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 6: {
            bulan = 'Juni'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 7: {
            bulan = 'Juli'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 8: {
            bulan = 'Agustus'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 9: {
            bulan = 'September'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 10: {
            bulan = 'Oktober'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 11: {
            bulan = 'November'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          case 12: {
            bulan = 'Desember'
            console.log('Tanggal ' + tanggal + ' bulan ' + bulan + ' tahun ' + tahun + '')
            break;
          }
          default: {
            console.log('Bulan yang anda masukan salah')
          }
        }
      }
    }
  }
}