// // Soal No 1
function arrayToObject(arr) {
  var now = new Date()
  var thisYear = now.getFullYear()
  newArray = {}

  for (i = 0; i <= arr.length - 1; i++) {
    age = thisYear - arr[i][3]
    if (age < 0 || arr[i][3] == null) {
      age = 'Invalid birth year'
    }
    objectBaru = {}
    objectBaru.firstName = arr[i][0]
    objectBaru.lastName = arr[i][1]
    objectBaru.gender = arr[i][2]
    objectBaru.age = age
    nama = arr[i][0] + ' ' + arr[i][1]
    newArray[nama]=objectBaru
  }
  return newArray
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
console.log(arrayToObject(people))
/*
  1. Bruce Banner: { 
      firstName: "Bruce",
      lastName: "Banner",
      gender: "male",
      age: 45
  }
  2. Natasha Romanoff: { 
      firstName: "Natasha",
      lastName: "Romanoff",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
console.log(arrayToObject(people2))
/*
  1. Tony Stark: { 
      firstName: "Tony",
      lastName: "Stark",
      gender: "male",
      age: 40
  }
  2. Pepper Pots: { 
      firstName: "Pepper",
      lastName: "Pots",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

// Error case 
console.log(arrayToObject([]))// ""
console.log('#############################################################')
console.log('#############################################################')
console.log('#############################################################')


// Soal No 2
function shoppingTime(memberId='', money='') {
  tampil ={}
  tampil.memberId = memberId
  tampil.money = money
  barang = {
    'Sepatu Stacattu' : 1500000 ,
    'Baju Zoro' : 500000 ,
    'Baju H&N' : 250000 ,
    'Sweater Uniklooh' : 175000 ,
    'Casing HP' : 50000
  }
  listPurchased=[]
  if (memberId == '' || money==''){
    tampil = 'Mohon maaf, toko X hanya berlaku untuk member saja'
  }
  else if (memberId && money<50000){
    tampil = 'Mohon maaf uang tidak cukup'
  }
  else if (memberId && money){
    switch(true){
      case money>=1500000 :
        listPurchased.push('Sepatu Stacattu')
        money = money - barang['Sepatu Stacattu']
      case money>=500000 :
        listPurchased.push('Baju Zoro')
        money = money - barang['Baju Zoro']
      case money>=250000 :
        listPurchased.push('Baju H&N')
        money = money - barang['Baju H&N']
      case money>=175000 :
        listPurchased.push('Sweater Uniklooh')
        money = money - barang['Sweater Uniklooh']
      case money>=50000 :
        listPurchased.push("Casing HP")
        money = money - barang["Casing HP"]
        break
    }
  }
  tampil.listPurchased = listPurchased
  tampil.changedMoney = money

  return tampil
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

/*Sepatu brand Stacattu seharga 1500000
Baju brand Zoro seharga 500000
Baju brand H&N seharga 250000
Sweater brand Uniklooh seharga 175000
Casing Handphone seharga 50000 */
console.log('#############################################################')
console.log('#############################################################')
console.log('#############################################################')

// Soal No 3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  for (i=0; i<=arrPenumpang.length-1;i++){
    
  }
  listBaru = []
  for (i=0; i<=arrPenumpang.length-1;i++){
    objectBaru={}
    awal=''
    akhir=''
    objectBaru.penumpang = arrPenumpang[i][0]
    objectBaru.naikDari = arrPenumpang[i][1]
    if (objectBaru.naikDari=='A'){
      awal = 1
    }
    else if (objectBaru.naikDari=='B'){
      awal = 2
    }
    else if (objectBaru.naikDari=='C'){
      awal = 3
    }
    else if (objectBaru.naikDari=='D'){
      awal = 4
    }
    else if (objectBaru.naikDari=='E'){
      awal = 5
    }
    else if (objectBaru.naikDari=='F'){
      awal = 6
    }
    objectBaru.tujuan = arrPenumpang[i][2]
    if (objectBaru.tujuan=='A'){
      akhir = 1
    }
    else if (objectBaru.tujuan=='B'){
      akhir = 2
    }
    else if (objectBaru.tujuan=='C'){
      akhir = 3
    }
    else if (objectBaru.tujuan=='D'){
      akhir = 4
    }
    else if (objectBaru.tujuan=='E'){
      akhir = 5
    }
    else if (objectBaru.tujuan=='F'){
      akhir = 6
    }
    objectBaru.bayar = 2000 * (akhir-awal)
    listBaru.push(objectBaru)

  }
  return listBaru
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]